#!/usr/bin/env python
"""Tic Tac Toe

The classic Tic Tac Toe game implemented in a CLI
offering a 2-Player game with score keeping and custom names.

Author: Vilian Atmadzhov
"""

import os
import sys
import sqlite3
import gamedb
import datetime
import texttable

AROW = [' ', ' ', ' ']
BROW = [' ', ' ', ' ']
CROW = [' ', ' ', ' ']
BOARD = [AROW, BROW, CROW]

CURRENT_PLAYER = 1
END_GAME = False

PLAYER1 = {'id' : '0', 'name': 'Player 1',  'symbol':'X',
'score': 0, 'winner' : 0}
PLAYER2 = {'id' : '0', 'name': 'Player 2',  'symbol':'O',
'score': 0, 'winner' : 0}

CONN = sqlite3.connect(sys.path[0] + '/game.db',
detect_types=sqlite3.PARSE_DECLTYPES)
gamedb.create_db(CONN)


def set_name(player_name):
    """Set the name of the current player

    Asks the player for a name and returns it.
    If blank, returns the old name.

    Argument: player_name [string]
    """
    tmp = raw_input('\n' + player_name +', enter your name:')
    if tmp != '':
        player_name = tmp
    return player_name

def draw_board(board):
    """Draw the board

    Draws the board from a list,holding 3 lists of 3 items.

    Argument: board [list]
    """
    print '\n'
    print '     || 1 | 2 | 3 |'
    print '  ===++===========+'
    print '   A ||', board[0][0], '|', board[0][1], '|', board[0][2], '|'
    print '  ---++---+---+---+'
    print '   B ||', board[1][0], '|', board[1][1], '|', board[1][2], '|'
    print '  ---++---+---+---+'
    print '   C ||', board[2][0], '|', board[2][1], '|', board[2][2], '|'
    print '  ---++---+---+---+'
    print '\n'

def show_board():
    """Shows the coordinates of each cell

    """
    print '\n'
    print '     || 1 | 2 | 3 |  This is the board.'
    print '  ===++===========+'
    print '   A || 7 | 8 | 9 |  To place a symbol,'
    print '  ---++---+---+---+  use either row-column'
    print '   B || 4 | 5 | 6 |  e.g. A1; B3; C2'
    print '  ---++---+---+---+'
    print '   C || 1 | 2 | 3 |  or the number in each cell.'
    print '  ---++---+---+---+'
    print '\n'

def clear_console():
    """Clear the console

    Clear the console output,using the corrent command
    for the appropriate OS
    """
    os.system('cls' if os.name == 'nt' else 'clear')

def clear_board(board, arow, brow, crow):
    """Clear the board

    Clears the board by changing the items in the lists with blanks

    Arguments:board, xrow, yrow, zrow [list]
    """
    for i in range(0, 3):
        arow[i] = ' '
        brow[i] = ' '
        crow[i] = ' '
    board = [arow, brow, crow]
    return board

def start_round(board,  player1,  player2,  current_player):
    """Start a new round

    Starts a new round by getting the board,
    players and the player to start first.

    Returns: winner of the game
    0 - Draw
    1 - player1
    2 - player2

    Arguments: board, player1, player2, current_player
    """
    count = 0
    game_end = False
    while game_end == False:
        if count < 9:
            if current_player == 1:
                player = player1
            else:
                player = player2
            tmp = make_move(board,  player)
            count += 1
            board = tmp[0]
            win = tmp[1]
            if win == True:
                game_end = True
            else:
                if current_player == 1:
                    current_player = 2
                else:
                    current_player  = 1
        else:
            game_end = True
            current_player = 0

        clear_console()
        draw_board(board)
    return current_player

def valid_move(board,  player_string):
    """Check the validity of the coordinates

    Loop until coordinates of empty cell are given.

    Returns: coords

    Arguments:board,player_name
    """
    valid = False
    while valid == False:
        coords = valid_input(player_string)
        if board[coords[0]][coords[1]] == ' ':
            valid = True
    return coords

def valid_input(player_name):
    """Get input and convert to board coordinates

    Returns: coords

    Arguments: player_name
    """
    valid = False
    coords = [0, 0]
    while valid == False:
        player_input = raw_input(player_name +
        ', enter desired coordinates:').lower()
        if len(player_input) == 2:
            row = player_input[0]
            col = player_input[1]
            if ((row == 'a' or row == 'b' or row == 'c') and
            (col == '1' or col == '2' or col == '3')):
                if row == 'a':
                    coords[0] = 0
                elif row == 'b':
                    coords[0] = 1
                else:
                    coords[0] = 2
                coords[1] = int(player_input[1]) - 1
                valid = True
        elif len(player_input) == 1:
            if player_input == '7':
                coords = [0, 0]
                valid = True
            elif player_input == '8':
                coords = [0, 1]
                valid = True
            elif player_input == '9':
                coords = [0, 2]
                valid = True
            elif player_input == '4':
                coords = [1, 0]
                valid = True
            elif player_input == '5':
                coords = [1, 1]
                valid = True
            elif player_input == '6':
                coords = [1, 2]
                valid = True
            elif player_input == '1':
                coords = [2, 0]
                valid = True
            elif player_input == '2':
                coords = [2, 1]
                valid = True
            elif player_input == '3':
                coords = [2, 2]
                valid = True

    return coords

def make_move(board,  player):
    """Get input from user,check validity of coordinates and make move,
    check for a winner[boolean]

    Return: board, win

    Arguments: board, player
    """
    coords = valid_move(board,  ('{0} ({1})').format(
    player['name'], player['symbol']))
    board[coords[0]][coords[1]] = player['symbol']
    win = check_win(board)
    return board,  win

def check_win(board):
    """Check for a winning pattern

    Return Boolean result
    """
    win = False
    if (board[0][0] == board[0][1] and
    board[0][1] == board[0][2] and board[0][0] != ' '):
        win = True
    elif (board[1][0] == board[1][1] and
    board[1][1] == board[1][2] and board[1][0] != ' '):
        win = True
    elif (board[2][0] == board[2][1] and
    board[2][1] == board[2][2] and board[2][0] != ' '):
        win = True
    elif (board[0][0] == board[1][0] and
    board[1][0] == board[2][0] and board[0][0] != ' '):
        win = True
    elif (board[0][1] == board[1][1] and
    board[1][1] == board[2][1] and board[0][1] != ' '):
        win = True
    elif (board[0][2] == board[1][2] and
    board[1][2] == board[2][2] and board[0][2] != ' '):
        win = True
    elif (board[0][0] == board[1][1] and
    board[1][1] == board[2][2] and board[0][0] != ' '):
        win = True
    elif (board[0][2] == board[1][1] and
    board[1][1] == board[2][0] and board[0][2] != ' '):
        win = True

    return win

def get_id(conn, name):
    found = False
    ########print '\n Getting ID for: ' + name
    while found == False:
        c = gamedb.get_player_id(conn, name)
        id = c.fetchone()
        if id[0] == 0:
            gamedb.add_player(conn, name)
        else:
            found = True
    ########print ('\nGot '+ str(id[1]) + ' for ' + name)
    return id[1]

def get_id2(conn, name2):
    found = False
    ########print '\n Getting ID2 for: ' + name2
    while found == False:
        c = gamedb.get_player2_id(conn, name2)
        id = c.fetchone()
        if id[0] == 0:
            gamedb.add_player(conn, name2)
        else:
            found = True
    #########print ('\nGot '+ str(id[1]) + ' for ' + name2)
    return id[1]

def print_game_score(p1name, p1score, p2score, p2name):
    print ('The score was {0} : {1} - {2} : {3}').format(
	p1name, p1score, p2score, p2name)

def print_top_table(conn):
      print 'Top 10'
      c = gamedb.get_top_players(conn)
      toplist = [["Player","Score","Points"]]
      for row in c:
          toplist.append(row)
      toptable = texttable.Texttable()
      toptable.set_cols_align(["l","r","r"])
      toptable.set_cols_valign(["c","c","c"])
      toptable.add_rows(toplist)
      print toptable.draw()

def print_rivalry(conn,p1,p2):     
      c = gamedb.get_rivalry(conn,p1,p2)
      print 'Rivalry:'
      rivalrylist = [["Player 1","Score","Score","Player 2","Ratio"]]
      for row in c:
          rivalrylist.append(row)
          
      rivalrytable = texttable.Texttable()
      rivalrytable.set_cols_align(["l", "r", "r","l","c"])
      rivalrytable.set_cols_valign(["m", "m", "m","m","m"])
      rivalrytable.add_rows(rivalrylist)
      print rivalrytable.draw()

clear_console()
print 'Welcome to Vivo\'s 2-Player Tic Tac Toe'
PLAYER1['name'] = set_name(PLAYER1['name'])
PLAYER2['name'] = set_name(PLAYER2['name'])
clear_console()
show_board()
print PLAYER1['name'] ,  \
', you will be playing with the', PLAYER1['symbol']
print PLAYER2['name'], ', that leaves you with the', PLAYER2['symbol']
print '\n', "As we all know that \"First Come,First Play\",\n", \
PLAYER1['name'] ,  'will start first.\n'


#print 'This is the board. To place your symbol,'
#print 'just enter the coordinates of the board e.g. A2 or C1.'
#print 'Alternatively, use the NumPad-each number matches a cell.'
#print 'Example: 7 = A1; 6 = B3; 2 = C2\n'

while END_GAME == False:
    clear_board(BOARD, AROW, BROW, CROW)
    WINNER = start_round(BOARD,  PLAYER1,  PLAYER2,  CURRENT_PLAYER)
    if CURRENT_PLAYER == 1:
        CURRENT_PLAYER = 2
        PLAYER = PLAYER2
    else:
        CURRENT_PLAYER = 1
        PLAYER = PLAYER1
    if WINNER == 1:
        PLAYER1['score'] += 1
        print PLAYER1['name'],  ', you WIN!'
    elif WINNER == 2:
        PLAYER2['score'] += 1
        print PLAYER2['name'],  ', you WIN!'
    else:
        print 'This round is a draw!'

    print ('The current score is {0} : {1} - {2} : {3}\n').format(
    PLAYER1['name'], PLAYER1['score'], PLAYER2['score'], PLAYER2['name'])
    print ('{0} will start the next round...\n').format(PLAYER['name'])
    VALID_ANSWER = False
    while VALID_ANSWER == False:
        if PLAYER1['score'] == PLAYER2['score']:
            PLAYER_INPUT = raw_input('No winner yet! Would you like' +
            ' to play \nanother round to see who is the winner? (Y/N)').lower()
            PLAYER1['winner'] = 0
            PLAYER2['winner'] = 0
        elif PLAYER1['score'] > PLAYER2['score']:
            PLAYER_INPUT = raw_input(('{0}, you seem to be losing... \n' +
            'Would you like another chance to beat {1}? (Y/N)').format(
            PLAYER2['name'], PLAYER1['name'])).lower()
            PLAYER1['winner'] = 1
            PLAYER2['winner'] = -1
        else:
            PLAYER_INPUT = raw_input(('{0}, you seem to be losing... \n' +
            'Would you like another chance to beat {1}? (Y/N)').format(
            PLAYER1['name'], PLAYER2['name'])).lower()
            PLAYER1['winner'] = -1
            PLAYER2['winner'] = 1
        VALID_LIST = ['y', 'yes', 'n', 'no']
        if PLAYER_INPUT in VALID_LIST:
            VALID_ANSWER = True
            clear_console()
            clear_board(BOARD, AROW, BROW, CROW)
            draw_board(BOARD)
    if PLAYER_INPUT == 'n' or PLAYER_INPUT == 'no':
        END_GAME = True
        clear_console()
        
        
        
        print_game_score(
        PLAYER1['name'], PLAYER1['score'],
        PLAYER2['score'], PLAYER2['name'])
        print 'I hope you enjoyed it!'
        ##New Database stuff
        ########print 'Here it goes funny...\n'

        PLAYER1['id'] = get_id(CONN, PLAYER1['name'])
        #########print '\nplayer 1 call is back...\n'
        #########print PLAYER1['name'], PLAYER1['id']
        PLAYER2['id'] = get_id2(CONN, PLAYER2['name'])

        ########print '\nWow, I survived...\n\n'
        P1 = [PLAYER1['id'],PLAYER1['score'],PLAYER1['winner']]
        P2 = [PLAYER2['id'],PLAYER2['score'],PLAYER2['winner']]
        date = datetime.datetime.now()
        gamedb.add_score(CONN, date, P1, P2)
        print 
	print_rivalry(CONN,PLAYER1['id'],PLAYER2['id'])
	print 
	print_top_table(CONN)

        raw_input('\nPress ENTER to continue...')
        clear_console()