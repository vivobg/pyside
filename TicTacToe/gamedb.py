import sqlite3
import datetime

def create_db(conn):
    createPlayer = """CREATE TABLE IF NOT EXISTS player(
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL)"""

    createGame = """CREATE TABLE IF NOT EXISTS game(
    id INTEGER PRIMARY KEY,
    played timestamp)"""

    createScore = """CREATE TABLE IF NOT EXISTS score(
    id INTEGER PRIMARY KEY,
    game_id INTEGER NOT NULL,
    player_id INTEGER NOT NULL,
    score INTEGER,winner INTEGER,
    FOREIGN KEY(game_id) REFERENCES game(id),
    FOREIGN KEY(player_id) REFERENCES player(id))"""

    createTop10View = """CREATE VIEW IF NOT EXISTS top10 AS
    SELECT name as Player, SUM(score) as Total , SUM(winner) as Points
    FROM score,player
    WHERE score.player_id = player.id
    GROUP BY player.id
    ORDER BY Total DESC
    LIMIT 10 """

    conn.execute(createPlayer)
    conn.execute(createGame)
    conn.execute(createScore)
    conn.execute(createTop10View)
    conn.commit()

def create_game(conn, date):
    conn.execute("INSERT INTO game VALUES(NULL,?)", (date, ))
    conn.commit()

def add_player(conn, name):
    ##########print '\nAdding player...' + name + '\n'
    conn.execute("INSERT INTO player VALUES(NULL,?)", (name, ))
    conn.commit()

def add_player2(conn, name):
    ##########print '\nAdding player...' + name + '\n'
    conn.execute("INSERT INTO player VALUES(NULL,?)", (name, ))
    conn.commit()

def get_player_id(conn, player_name):
    ########print player_name , 'This is get_player_id'
    cur = conn.cursor()
    ##########print '\nCursor is set...\nExecuting cursor...\n'
    cur.execute("SELECT COUNT(id), id,  name FROM player WHERE name=:name", {
    "name": player_name})
    ##########print '\n Cursor executed...\n'
    return cur

def get_player2_id(conn, player_name):
    ##########print player_name , 'This is get_player_id2'
    cur = conn.cursor()
    ##########print '\nCursor2 is set...\nExecuting cursor2...\n'
    cur.execute("SELECT COUNT(id), id,  name FROM player WHERE name=:name2", {
    "name2": player_name})
    ##########print '\n Cursor2 executed...\n'
    return cur

def add_score(conn, date, player1, player2):
    create_game(conn, date)

    conn.execute("""INSERT INTO score VALUES(NULL,
    (SELECT MAX(id) FROM game), ?, ?, ?)""", (
    player1[0], player1[1], player1[2]))

    conn.execute("""INSERT INTO score VALUES(NULL,
    (SELECT MAX(id) FROM game), ?, ?, ?)""", (
    player2[0], player2[1], player2[2]))

    conn.commit()

def get_top_players(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM top10")
    return cur

def get_rivalry(conn, player1, player2):
    cur = conn.cursor()
    cur.execute("""SELECT player1.name as Player, SUM(Pscore.score) as PScore,
    SUM(Oscore.score) as OScore,player2.name as Opponent,
    ROUND(SUM(Pscore.score) * 1.0 / SUM(Oscore.score), 2) as Ratio
    FROM player as player1, player as player2,score as Pscore,
    score as Oscore, game
    WHERE player1.id = Pscore.player_id AND
    player2.id = Oscore.player_id AND
    player1.id <> player2.id AND
    Pscore.game_id = Oscore.game_id AND
    game.id = Pscore.game_id AND
    game.id = Oscore.game_id
    AND player1.id = ?
    AND player2.id = ? """, (player1, player2))
    return cur