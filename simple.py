#!/usr/bin/python2
import PySide
from PySide import QtCore, QtGui
from ui_simple import Ui_MainWindow
from ui_about import Ui_Dialog
import platform,random

__version__ = "0.1 alpha"

label = ''
size=2
col = ['red','green','blue','yellow','black','purple','cyan','white']
x_size=600
y_size=600
def drawImage():
	#image = QtGui.QImage("./simple.png")
	pixmap = QtGui.QPixmap(x_size,y_size)
	pixmap.fill()
	painter = QtGui.QPainter(pixmap)
	#painter.setPen(QtGui.QColor(100,100,100))
	painter.setBrush(QtGui.QColor(255, 0, 0))
	#painter.begin()
	painter.fillRect(0,0,50,50,QtGui.QColor('red'))
	painter.drawRect(50,50,100,100)
	#painter.setPen(QtGui.QColor("blue"))
	painter.drawLine(123,123,205,205)
	for x in range(0,x_size,size):
		for y in range(0,y_size,size):
			#painter.setBrush(QtGui.QColor(x if x<256 else 255,0,y if y<256 else 255))
			painter.setBrush(QtGui.QColor(col[random.randint(0,len(col)-1)]))
			#painter.setBrush(QtGui.QColor( random.randint(0,255),random.randint(0,255),random.randint(0,255)  ))
			painter.drawRect(x,y,size,size)
	painter.end()
	label.setPixmap(pixmap)

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    main = Ui_MainWindow()
    main.setupUi(MainWindow)
    
    MainWindow.show()
    Dialog = QtGui.QDialog()
    about = Ui_Dialog()
    about.setupUi(Dialog)
    about.textEdit.setText("""<b>About the app</b><br/>
		Version: %s<br/>
		Python version: %s<br/>
		PySide version: %s<br/>
		Qt version: %s<br/>
		System: %s""" % (__version__,platform.python_version() ,PySide.__version__,QtCore.__version__,platform.system()))
    main.btnAbout.clicked.connect(Dialog.show)
    main.actionAbout.activated.connect(Dialog.show)
    main.btnDraw.clicked.connect(drawImage)
    main.labelDraw.setMinimumSize(x_size,y_size)
    label = main.labelDraw
    sys.exit(app.exec_())

