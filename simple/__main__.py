#!/usr/bin/python2
import PySide
from PySide import QtCore, QtGui
from ui_simple import Ui_MainWindow
from ui_about import Ui_Dialog
import platform

__version__ = "0.1 alpha"

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    main = Ui_MainWindow()
    main.setupUi(MainWindow)
    
    MainWindow.show()
    Dialog = QtGui.QDialog()
    about = Ui_Dialog()
    about.setupUi(Dialog)
    about.textEdit.setText("""<b>About the app</b><br/>
		Version: %s<br/>
		Python version: %s<br/>
		PySide version: %s<br/>
		Qt version: %s<br/>
		System: %s""" % (__version__,platform.python_version() ,PySide.__version__,QtCore.__version__,platform.system()))
    main.btnAbout.clicked.connect(Dialog.show)
    main.actionAbout.activated.connect(Dialog.show)
    sys.exit(app.exec_())

