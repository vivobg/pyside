#!/usr/bin/python2
import sys
from PySide.QtCore import *
from PySide.QtGui import *

class Form(QDialog):
	def __init__(self,parent=None):
		super(Form,self).__init__(parent)
		self.setWindowTitle("MyForm")
		self.edit = QLineEdit("Write my name here")
		self.button = QPushButton("Show")
		self.button.clicked.connect(self.greetings)
		self.exitButton = QPushButton("Exit")
		self.exitButton.clicked.connect(self.close)
		layout = QVBoxLayout() #QGridLayout
		#lay2 = QHBoxLayout()
		#lay2.addWidget(self.button)
		#lay2.addWidget(self.exitButton)
		layout.addWidget(self.edit)
		layout.addWidget(self.button)
		layout.addWidget(self.exitButton)
		self.setLayout(layout)
		
	def greetings(self):
		self.label = QLabel("Hello %s" % self.edit.text())
		self.label.show()
		
def sayHello():
	print "Hello World"
	


if __name__ == '__main__':
	
	app = QApplication(sys.argv)
	form = Form()
	form.show()
	
	#label = QLabel("Hello World!")
	#label.show()
	
	#button = QPushButton("Click me")
	#button.clicked.connect(sayHello)
	#button.show()
	
	exit(app.exec_())






